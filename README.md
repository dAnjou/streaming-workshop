# Streaming
Prerequisites:
- `/usr/bin/python3`
- Optionally for WebSocket example:
  - [websocketd](http://websocketd.com) or Docker
  - [websocat](https://github.com/vi/websocat)
---
Optional configuration of sluggishness:
```shell
export FETCHING_TIME=0.5
export PROCESSING_TIME=0.5
```
---
Fetching data from a slow-ish data source:
```shell
./fetch_data.py
```
Fetching and processing while keeping the data completely in memory:
```shell
./fetch_data.py | ./bulk_processing.py
```
Fetching data completely into memory but streaming the processed results:
```shell
./fetch_data.py | ./naive_processing.py
```
Fetching and processing data as a stream:
```shell
./fetch_data.py | ./stream_processing.py
```
---
Everything as a web service using WebSocket:
```shell
websocketd --staticdir=. --address=localhost --port=8080 --dir=programs
```
or
```shell
docker build -t streaming . && docker run -it -p 8080:8080 streaming
```
Open http://localhost:8080 in a browser, or use the CLI client:
```shell
websocat ws://127.0.0.1:8080/bulk.sh
```
```shell
websocat ws://127.0.0.1:8080/naive.sh
```
```shell
websocat ws://127.0.0.1:8080/stream.sh
```
