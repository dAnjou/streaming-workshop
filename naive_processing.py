#!/usr/bin/python3

from sys import stdin

from process import process
from utils import start, end, p_out

start(__file__)
### SNIP
for value in stdin.readlines():
    p_out(process(value))
### SNIP
end()
