import json
import os
from time import sleep

from utils import time, getenv


### SNIP
def process(value):
    sleep(getenv("PROCESSING_TIME", 0.3, float))
    data = json.loads(value)
    data["processed"] = time()
    return json.dumps(data)
### SNIP
