#!/usr/bin/python3

from sys import stdin

from process import process
from utils import start, end, p_out

start(__file__)
### SNIP
results = []

for value in stdin.readlines():
    results.append(process(value))

p_out("\n".join(results))
### SNIP
end()
