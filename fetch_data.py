#!/usr/bin/python3

import json
from time import sleep

from utils import start, end, p_out, time, getenv

start(__file__)
### SNIP
for value in range(10):
    sleep(getenv("FETCHING_TIME", 1, float))
    p_out(json.dumps(dict(value=value, fetched=time())))
### SNIP
end()
