document.querySelector("#all").addEventListener("click", () => {
    document.querySelectorAll("table button").forEach(element => element.click());
});

document.querySelectorAll("table button").forEach(element => element.addEventListener("click", () => {
    let start = undefined;
    const tbody = element.closest("table").querySelector("tbody");
    tbody.innerHTML = "";
    const ws = new WebSocket('ws://localhost:8080/' + element.dataset.program + '.sh');
    ws.onopen = function() {
        start = new Date();
        element.closest("caption").querySelector("span").innerHTML = "Started: " + start.toLocaleTimeString();
    };
    ws.onmessage = function(event) {
        const data = JSON.parse(event.data);
        const fetched = new Date(data.fetched);
        const processed = new Date(data.processed);
        const element = document.createElement('tr');
        element.innerHTML = "<th>" + data.value + "</th>"
            + "<td>" + fetched.toLocaleTimeString() + "</td>"
            + "<td>" + processed.toLocaleTimeString() + "</td>"
            + "<td>" + (new Date() - start) / 1000 + "</td>";
        tbody.appendChild(element);
    };
}))

document.querySelectorAll(".code").forEach(element => {
    fetch(element.dataset.program)
        .then(response => response.text())
        .then(data => {
            data.split('### SNIP')
                .filter((_, index) => index % 2 !== 0)
                .forEach(snippet => {
                    const pre = document.createElement("pre");
                    pre.innerHTML = "<p>" + element.dataset.program + "</p>"
                        + "<code>" + snippet.trim() + "</code>";
                    element.appendChild(pre);
                });
        })
})
