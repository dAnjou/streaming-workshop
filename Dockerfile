FROM python:3

RUN apt-get update \
    && apt-get -y upgrade \
    && apt-get -y install --no-install-recommends \
      websocketd

COPY programs programs/
COPY index.html style.css script.js *.py ./

CMD ["websocketd", "--staticdir=.", "--address=0.0.0.0", "--port=8080", "--dir=programs"]
