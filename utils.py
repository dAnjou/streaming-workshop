import datetime
import os
from pathlib import Path
from sys import stderr, stdout

_start = None
_file = None


def p_out(value):
    print(value, file=stdout, flush=True)


def p_err(value):
    print(value, file=stderr, flush=True)


def time():
    return datetime.datetime.now().isoformat(timespec="seconds")


def start(file):
    global _file
    global _start
    _file = file
    _start = datetime.datetime.now()
    p_err(f"{Path(_file).name}: {_start.time().isoformat('seconds')}")


def end():
    global _file
    global _start
    t = datetime.datetime.now() - _start
    p_err(f"{Path(_file).name}: {t.seconds} seconds")


def getenv(value, default=None, cast=lambda v: v):
    return cast(os.getenv(value, default))
